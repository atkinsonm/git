# Git

This project is designed to implement general Git project best practice checks as GitLab CI pipeline jobs. The purpose is to improve the quality of your projects and ensure that contributors are meeting minimum standards.

## Components

This project is comprised of two components:

1. [templates](templates) which can be `include`d in another project's `.gitlab-ci.yml` file to implement the CI jobs described below.
2. A [Docker](Dockerfile) image containing dependencies for the CI jobs.

## Using this Template

1. Add this code to the top of your project's `.gitlab-ci.yml`:

    ```yml
    include:
      - https://gitlab.com/jhctechnology/code-quality/Git/raw/v0.4/templates/.gitlab-ci-git.yml
    ```

    You may also substitute any tag or branch for `master` to specify a version other than the latest release.

2. Ensure your `.gitlab-ci.yml` file `stages` includes a stage called `qa`.

That's it! If you would like to modify the behavior of any included jobs, override them by defining them in your `.gitlab-ci.yml` file after the `include` statement.

## Template Jobs

### [qa:check_changelog_edits](templates/.gitlab-ci-git.yml)

Runs on merge request pipelines. Checks `git diff` for edits to `CHANGELOG.md` between the current branch and the master branch. Exits with a warning (`allow_failure: true`) if no changes exist.

### [qa:check_readme_edits](templates/.gitlab-ci-git.yml)

Runs on merge request pipelines. Checks `git diff` for edits to `README.md` between the current branch and the master branch. Exits with a warning (`allow_failure: true`) if no changes exist.

### [qa:check_top_level_changelog](templates/.gitlab-ci-git.yml)

Checks for the existence of a file called CHANGELOG.md at the root of the project. Exits with failure if it does not exist.

### [qa:check_top_level_readme](templates/.gitlab-ci-git.yml)

Checks for the existence of a file called README.md at the root of the project. Exits with failure if it does not exist.

### [qa:markdownlint](templates/.gitlab-ci-git.yml)

Runs [markdownlint](https://www.npmjs.com/package/markdownlint) to check all markdown files (any file with extension `.md`) for general markdown best practices. Exits with failure if any errors are found. Uses the style defined in [relaxed.json](style/relaxed.json) by default. To change any behaviors, create a file called `markdownlint.js` in the root of your project.

Runs only on important refs (tagged refs, master branch, merge requests into master) or when at least one file with extension `.md` has changed in the diff. This helps reduce the number of running jobs when they are not relevant.

## Dependencies/Credits

All credit for the following dependencies goes to their respective developers and owners:

* [markdownlint](https://www.npmjs.com/package/markdownlint)
