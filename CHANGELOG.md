# CHANGELOG

## v0.4

* break dependence on other jhctech-oss projects

## v0.3

* add merge request CI job to check for edits to README.md between the current branch and master

## v0.2

* add merge request CI job to check for edits to CHANGELOG.md between the current branch and master

## v0.1

* initial release
